﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoter : MonoBehaviour
{
    private static LayerMask Snack;
    private Vector3 direction;
    float time = 2f;
    float force = 1f;
    bool good = false;
    // Start is called before the first frame update
    void Awake()
    {
        Snack = LayerMask.NameToLayer("Snack");
        float time = 2f;
        good = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(time > 0)
        time -= Time.deltaTime;
        if (direction != null)
            Movement();
    }

    public void Movement()
    {
        Vector3 currentPosition = transform.position;
        Vector3 deltaPosition = (
            direction * force
        );
        transform.position = currentPosition + deltaPosition;
        if (Cage(transform.position) && force > 0)
            force -= 0.001f;
        else
            force = 0;
    }

    public void Stop()
    {
        force = 0;
    }

    public void deplacement(float force, Vector3 direction)
    {
        this.direction = direction;
        this.force = force;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (time < 0)
        {
            if (!good)
                if (collision.gameObject.layer == Snack)
                {
                    Debug.Log("Snack");
                    good = true;
                    Destroy(gameObject);
                    gameObject.GetComponentInParent<ControllerBodyShot>().ShotGood();
                }
        }
        //Check si il y a pas d'autre balle et les relier ensemble, pareil pour les bonbon
    }

    private bool Cage(Vector3 position)
    {
        if (position.x < -24 ||
            position.y < 1 ||
            position.z < -24 ||
            position.x > 24 ||
            position.y > 49 ||
            position.z > 24)
            return false;
        return true;
    }

}
