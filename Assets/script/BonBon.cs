﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonBon : MonoBehaviour
{
    private static LayerMask Snack;
    private static LayerMask Shoter;
    private float repeatTime = 5;
    private Vector3 deltaPosition;
    private bool dead = false;

    // Start is called before the first frame update
    void Awake()
    {
        Snack  = LayerMask.NameToLayer("Snack");
        Shoter = LayerMask.NameToLayer("Shoter");
        InvokeRepeating("setMovement", 0.0f, repeatTime);
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    private void setMovement()
    {
        deltaPosition = new Vector3(Random.Range(-0.01f, 0.01f),
                                    Random.Range(-0.01f, 0.01f),
                                    Random.Range(-0.01f, 0.01f));
        transform.rotation = Quaternion.Euler(deltaPosition) * Quaternion.Euler(0, -90, -90);
    }

    public void Movement()
    {
        Vector3 currentPosition = transform.position;
        if (Cage(currentPosition + deltaPosition))
            transform.position = currentPosition + deltaPosition;
        else
            setMovement();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!dead)
        {
            if (collision.gameObject.layer == Snack)
            {
                Debug.Log("Snack");
                dead = true;
                gameObject.GetComponentInParent<ControllerBonBon>().removeBonBon(gameObject);
            }
            if (collision.gameObject.layer == Shoter)
            {
                Debug.Log("Shoter");
                collision.gameObject.GetComponent<Shoter>().Stop();
                dead = true;
                gameObject.GetComponentInParent<ControllerBonBon>().removeBonBon(gameObject);
            }
        }
    }

    private bool Cage(Vector3 position)
    {
        if (position.x < -24 ||
            position.y < 1 ||
            position.z < -24 ||
            position.x > 24 ||
            position.y > 49 ||
            position.z > 24)
            return false;
        return true;
    }

}
