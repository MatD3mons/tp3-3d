﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerSnack : MonoBehaviour
{
    [SerializeField] private GameObject CameraScene = default;
    [SerializeField] private GameObject Tetes = default;
    [SerializeField] private GameObject membres = default;
    [SerializeField] private ControllerBodyShot BodyShot = default;

    private List<GameObject> Body = new List<GameObject>();
    public GameObject Head = null;
    // Start is called before the first frame update
    void Awake()
    {
        AddHandle();
        for(int i =0; i < 5; i++)
        {
           AddMember(last());
        }
    }

    // Update is called once per frame
    void Update()
    {
        HandleAction();
    }

    public GameObject last()
    {
        if (Body.Count > 0)
            return Body[Body.Count - 1];
        return Head;
    }

    private void HandleAction()
    {
        if (Input.GetButtonUp("Fire1"))
            if(Body.Count >= 1)
            {
                RemoveMember();
                BodyShot.Shot();
            }
    }

    private void RemoveMember()
    {
        GameObject fin = Body[Body.Count - 1];
        Body.Remove(fin);
        Destroy(fin);
    }

    private void AddHandle()
    {
        Head = Instantiate(Tetes, CameraScene.transform.position,Quaternion.identity,transform);
    }

    public void AddMember(GameObject avant)
    {
        GameObject membre = Instantiate(membres,
            avant.transform.position,
            Quaternion.identity,
            transform);
        membre.GetComponent<CharacterJoint>().connectedBody = avant.GetComponent<Rigidbody>();
        Body.Add(membre);
    }
}
