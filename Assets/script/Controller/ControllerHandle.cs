﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHandle : MonoBehaviour
{
    [SerializeField] public Transform CameraScene = default;
    [SerializeField] private float speed = 1f;
    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HandleMovement();
        CameraMove();
    }

    void CameraMove()
    {
        if(CameraScene != null)
        {
            transform.rotation = CameraScene.transform.rotation * Quaternion.Euler(0, -90, -90);
            CameraScene.transform.position = transform.position;
        }
    }
        
    private void HandleMovement()
    {
        Vector3 currentPosition = transform.position;
        Vector3 deltaPosition = (
            CameraScene.right * Input.GetAxis("Horizontal") * Time.deltaTime * speed
            + CameraScene.transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * speed
        ) * speed;
        transform.position = currentPosition + deltaPosition;
    }

}
