﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerBonBon : MonoBehaviour
{
    [SerializeField] private ControllerSnack ManagerSnack = default;
    [SerializeField] private List<GameObject> ListOfBonbon = default;
    [SerializeField] private float repeatTime = 10;
    [SerializeField] private float numberOfBonbon = 10;

    private List<GameObject> listofBonBon = new List<GameObject>();
    // Start is called before the first frame update
    void Awake()
    {
        InvokeRepeating("Spawn", 0.0f, repeatTime);
    }

    private void Spawn()
    {
        if(listofBonBon.Count < numberOfBonbon)
        {
            GameObject BonBon = ListOfBonbon[Random.Range(0, ListOfBonbon.Capacity)];
            Vector3 deltaPosition = new Vector3(Random.Range(-24f, 24f),
                                          Random.Range(1f, 49f),
                                          Random.Range(-24f, 24f));
            listofBonBon.Add(Instantiate(BonBon, deltaPosition, Quaternion.identity, transform));
        }
    }

    public void removeBonBon(GameObject gameObject)
    {
        listofBonBon.Remove(gameObject);
        GameObject avant = ManagerSnack.last();
        ManagerSnack.AddMember(avant);
        Destroy(gameObject);
    }

}
