﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerCamera : MonoBehaviour
{
    [SerializeField] private float mouseSensitivity = 1f;
    [SerializeField] public Transform ManagerSnack = default;
    [SerializeField] private float speed = 1f;
    private float yawn = 0.0f;
    private float pitch = 0.0f;

    // Start is called before the first frame update
    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        yawn = 0.0f;
        pitch = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {   
        CameraMovement();
        Movement();
        HeadMove();
    }

    private void CameraMovement()
    {
        yawn += Input.GetAxis("Mouse X") * mouseSensitivity;
        pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        pitch = Mathf.Clamp(pitch, -90f, 90f);
        transform.eulerAngles = new Vector3(pitch, yawn, 0f);
    }

    void HeadMove()
    {
        ManagerSnack.GetComponent<ControllerSnack>().Head.transform.rotation = transform.rotation * Quaternion.Euler(0, -90, -90);
        //TODO faire plusieur point de vue x)
        ManagerSnack.GetComponent<ControllerSnack>().Head.transform.position = transform.position - transform.forward*1f;
    }

    private void Movement()
    {
        Vector3 currentPosition = transform.position;
        Vector3 deltaPosition = (
            transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * speed
            + transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * speed
        );
        if(Cage(currentPosition + deltaPosition))
            transform.position = currentPosition + deltaPosition;
    }

    private bool Cage(Vector3 position)
    {
        if (position.x < -24 ||
            position.y < 1 ||
            position.z < -24 ||
            position.x > 24 ||
            position.y > 49 ||
            position.z > 24)
            return false;
        return true;
    }
}
