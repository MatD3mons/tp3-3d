﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerBodyShot : MonoBehaviour
{
    [SerializeField] private GameObject membres = default;
    [SerializeField] private GameObject CameraScene = default;
    [SerializeField] private ControllerSnack ManagerSnack = default;
    [SerializeField] private float spawnForce = 100f;
    private Transform MemberSpawn;
    // Start is called before the first frame update
    void Awake()
    {
        MemberSpawn = CameraScene.transform;
    }

    public void Shot()
    {
        //TODO faire voler le corps, et l'arrêter dans l'aire
        GameObject spawnedBall = Instantiate(membres, MemberSpawn.position, Quaternion.identity, transform); 
        spawnedBall.transform.rotation = CameraScene.transform.rotation * Quaternion.Euler(0, -90, -90);
        spawnedBall.GetComponent<Shoter>().deplacement(spawnForce, MemberSpawn.forward);
    }

    public void ShotGood()
    {
        //rajouter au snack et remove
        GameObject avant = ManagerSnack.last();
        ManagerSnack.AddMember(avant);
    }

}
